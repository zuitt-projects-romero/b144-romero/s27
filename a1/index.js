const http=require('http');




const port=4000;

// No. 3 

let users = [
	{
		"firstName": "Mary Jane",
		"lastName": "Dela Cruz",
		"mobileNo": "09123456789",
		"email": "mjdelacruz@mail.com",
		"password": 123
	},
	{
		"firstName": "John",
		"lastName": "Doe",
		"mobileNo": "09123456789",
		"email": "jdoe@mail.com",
		"password": 123
	}
]

const server=http.createServer((req, res) => {

// No. 4
		if(req.url == '/profile' && req.method == "GET"){
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.write(JSON.stringify(users));
		res.end();
	}

	
	if(req.url == '/profile' && req.method == "POST"){

		let requestBody = ''

		req.on('data', function(data){
			requestBody += data;
		})

		req.on('end', function(){

			console.log(typeof requestBody)

			requestBody=JSON.parse(requestBody)

			let newUser = {
				"firstName": requestBody.firstName,
				"lastName": requestBody.lastName,
				"mobileNo": requestBody.mobileNo,
				"email": requestBody.email,
				"password": requestBody.password
			}

			users.push(newUser)
			console.log(users)

			res.writeHead(200, {'Content-Type': 'application/json'} )
			res.write(JSON.stringify(newUser));
			res.end()
		})
	}

}).listen(4000);



console.log(`Server running at localhost:${port}.`)
