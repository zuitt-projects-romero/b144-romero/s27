const http = require('http')

const port = 4000;

const server = http.createServer((req, res) =>{

	// The HTTP method of the incoming request can be accessed via the "method" property of the "request" parameter

	// GET method will retrieve data or read an information

	// The Default method in http is GET method
	if(req.url == '/items' && req.method == "GET"){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Data retrieved from the database')
	}
	if(req.url == '/items' && req.method == "POST"){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Data to be sent to the database')
	}
	if(req.url == '/updateItem' && req.method == "PUT"){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Update our resources')
	}
	if(req.url == '/deleteItem' && req.method == "DELETE"){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Delete our resources')
	}

})

3
server.listen(port);

console.log(`Server now accessible localhost: ${port}.`)